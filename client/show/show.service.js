(function () {
    angular
        .module("DepartmentApp")
        .service("ShowService", ShowService);
    ShowService.$inject = ["$http", "$q"];
    function ShowService($http, $q) {
        var vm = this;
        
        vm.deleteManager=function (deptNo, managerId) {
            var defer = $q.defer();
            $http.delete("/api/managers/" + deptNo + "/" + managerId)
                .then(function (response) {
                    // This is a good way to understand the type of results you're getting
                    defer.resolve(response);
                    // Calls search() in order to populate manager info with predecessor of deleted manager
                    search();
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > deleteManager() > error: \n" + JSON.stringify(err));
                });
            return (defer.promise);
        }

        vm.save=function (deptNo,deptName) {
            var defer = $q.defer();
            $http.put("/api/departments/" + deptNo,
                {
                    dept_no: deptNo,
                    dept_name: deptName
                })
                .then(function (response) {
                    defer.resolve(response);
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > save() > error: \n" + JSON.stringify(err));
                });
            return (defer.promise);
        }
        
        vm.search=function (deptNo) {
            var defer = $q.defer();
            $http.get("/api/departments/" + deptNo)
                .then(function (response) {
                    defer.resolve(response);
                })
                .catch(function (err) {
                   
                });
            return (defer.promise);
        }
    }
})();