(function () {
    'use strict';
    angular
        .module("DepartmentApp")
        .controller("ShowCtrl", ShowCtrl);

    ShowCtrl.$inject = ["$http", "$filter","$stateParams","ShowService"];
    function ShowCtrl($http, $filter,$stateParams,ShowService) {
        console.log("-- in show.controller.js")
        // Exposed variables
        var vm = this;

        vm.dept_no = "";
        vm.result = {};

        // Exposed functions
        vm.deleteManager = deleteManager;
        vm.toggleEditor = toggleEditor;
        vm.initDetails = initDetails;
        vm.save = save;
        vm.search = search;

        // Initial calls
        initDetails();


        // Initializes department details shown in view
        function initDetails() {
            console.log("-- show.controller.js > initDetails()");
            vm.result.dept_no = "";
            vm.result.dept_name = "";
            vm.result.manager_id = "";
            vm.result.manager_name = "";
            vm.result.manager_from = "";
            vm.result.manager_to = "";
            vm.result.manager_id = "";
            vm.result.manager_name = "";
            vm.result.manager_from = "";
            vm.result.manager_to = "";
            vm.showDetails = false;
            vm.isEditorOn = false;

        }

        // Function definition
        // Deletes displayed manager. Details of preceding manager is then displayed.
        function deleteManager() {
            console.log("-- show.controller.js > deleteManager()");
            ShowService.deleteManager(vm.result.dept_no,vm.result.manager_id)
                .then(function (response) {
                    console.log("-- show.controller.js > deleteManager() > res obj after delete req: \n" + response);
                })
                .catch((function (error) {

                }))
        }


        // Saves edited department name
        function save() {
            console.log("-- show.controller.js > save()");
            ShowService.save(vm.dept_no,vm.result.dept_name)
                .then(function (response) {
                    console.log("-- show.controller.js > save() > results: \n" + JSON.stringify(response.data));
                })
                .catch(function (error) {
                    console.log("--  show.controller.js > save() > error: \n" + JSON.stringify(err));
                })
            vm.toggleEditor();
        }

        // Given a department number, this function searches the Employees database for
        // the department name, and the latest department manager's id/name and tenure
        function search() {
            console.log("-- show.controller.js > search()");
            initDetails();
            vm.showDetails = true;
            ShowService.search(vm.dept_no)
                .then(function (response) {
                    // Show table structure
                    vm.showDetails = true;

                    // This is a good way to understand the type of results you're getting
                    console.log("-- show.controller.js > search() > results: \n" + JSON.stringify(response.data));



                    // Exit .then() if response data is empty
                    if(!response.data)
                        return;

                    // The response is an array of objects that contain only 1 object
                    // We are assigning value like so, so that we don't have to do access complex structures
                    // from the view. Also this would give you a good sense of the structure returned.
                    // You could, of course, massage data from the back end so that you get a simpler structure
                    vm.result.dept_no = response.data.dept_no;
                    vm.result.dept_name = response.data.dept_name;
                    if (response.data.managers[0]) {
                        vm.result.manager_id = response.data.managers[0].emp_no;
                        vm.result.manager_name = response.data.managers[0].employee.first_name
                            + " "
                            + response.data.managers[0].employee.last_name;
                        vm.result.manager_from = $filter('date')
                        (response.data.managers[0].from_date, 'MMM dd, yyyy');
                        vm.result.manager_to = $filter('date')
                        (response.data.managers[0].to_date, 'MMM dd, yyyy');
                    }
                })
                .catch(function (error) {
                    console.log("--  show.controller.js > search() > error: \n" + JSON.stringify(err));
                })

        }

        // Switches editor state of the department name input/edit field
        function toggleEditor() {
            vm.isEditorOn = !(vm.isEditorOn);
        }

        if($stateParams.deptNo){
            vm.dept_no=$stateParams.deptNo;
            vm.search();
        }
    }
})();