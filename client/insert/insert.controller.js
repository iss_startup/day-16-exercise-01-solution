(function () {
    'use strict';
    
    angular
        .module("DepartmentApp")
        .controller("InsertCtrl", InsertCtrl);

    InsertCtrl.$inject = ["$http", "$filter","InsertService"];

    function InsertCtrl($http, $filter,InsertService) {
        console.log("-- insert.controller.js");
        
        // Exposed variables
        var vm = this;
        vm.department = {};
        vm.status = {
            message: "",
            success: ""
        };

        // Exposed functions
        vm.register = register;

        // Initial calls
        initEmployees();

        // Function definition
        function initEmployees(){
            console.log("-- insert.controller.js > initEmployees()");
            InsertService.intiEmployeeDepartments()
                .then(function (response) {
                    vm.employees = response;
                })
                .catch(function (error) {
                    
                })
        }

        function register() {
            // We need to add selected employee's start and end date as department manager
            vm.department.from_date = $filter('date')(new Date(), 'yyyy-MM-dd');
            vm.department.to_date = new Date('9999-01-01');
            console.log("vm.dept" + JSON.stringify(vm.department));

            InsertService.register(vm.department)
                .then(function (response) {
                    vm.employees = response;
                    vm.status.message = "The department is added to the database.";
                    vm.status.success = "ok";
                })
                .catch(function (error) {
                    vm.status.message = "Failed to add the department to the database.";
                    vm.status.success = "error";
                }) ;
            
            
        }
    } // END InsertCtrl

})();






