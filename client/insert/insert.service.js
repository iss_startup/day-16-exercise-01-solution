(function () {
    angular
        .module("DepartmentApp")
        .service("InsertService",InsertService);
    InsertService.$inject=["$http","$q"];
    function  InsertService($http,$q) {
        var vm=this;


        vm.register=function (departmentInfo) {
          var defer = $q.defer();
            $http.post("/api/departments",departmentInfo)
                .then(function (response) {
                    
                   defer.resolve(response);
                    console.info("Success! Response returned: " + JSON.stringify(response.data));
                  
                })
                .catch(function (err) {
                    defer.reject(err);
                    console.info("Error: " + JSON.stringify(err));
                   
                });
           return (defer.promise);
        }

        vm.intiEmployeeDepartments=function (departmentInfo) {
            var defer = $q.defer();
            $http.get("/api/employees")
                .then(function (response) {
                    console.info("-- insert.controller.js > initEmployees then() \n" + JSON.stringify(response.data));
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    defer.reject(err);
                    console.log("--  insert.controller.js > initEmployees > error: \n" + JSON.stringify(err));
                });
            return (defer.promise);
        }
    }
})();