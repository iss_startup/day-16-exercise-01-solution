(function () {
    angular
        .module("DepartmentApp")
        .service("SearchService", SearchService);
    SearchService.$inject = ["$http", "$q"];
    function SearchService($http, $q) {
        var vm = this;

        vm.initEmployeeData = function () {
            var defer = $q.defer();
            $http
                .get("/api/departments")
                .then(function (response) {
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    cosole.log("error " + err);
                });
            return (defer.promise);
        }

        vm.search = function (deptData) {
            var defer = $q.defer();
            $http
                .get("/api/departments",
                    deptData
                )
                .then(function (response) {
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    console.info("Error " + err);
                });
            return (defer.promise);
        }

        vm.searchManager = function (deptData) {
            var defer = $q.defer();
            $http.get("/api/managers",
                deptData
                )
                .then(function (response) {
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    console.info("Error " + err);
                });
            return (defer.promise);
        }
    }
})();