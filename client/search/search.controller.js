(function () {
    angular
        .module("DepartmentApp")
        .controller("SearchCtrl", SearchCtrl);

    SearchCtrl.$inject = ["$http", "$state", "SearchService"];

    function SearchCtrl($http, $state, SearchService) {
        var vm = this;

        vm.deptName = '';
        vm.result = null;
        vm.showManager = false;

        // Exposed functions
        vm.search = search;
        vm.searchForManager = searchForManager;

        // Initial calls
        init();


        // Function definitions
        function init() {
            SearchService.initEmployeeData()
                .then(function (response) {
                    vm.departments = response;
                })
                .catch(function (error) {
                    cosole.log("error " + error);
                })
        }

        function search() {
            vm.showManager = false;
            SearchService.search( {
                params: {
                    'dept_name': vm.deptName
                }
            }).then(function (response) {
                vm.departments = response;
            }).catch(function (error) {
                console.log(error);
            })
        }

        function searchForManager() {
            vm.showManager = true;


            SearchService.search( {
                params: {
                    'dept_name': vm.deptName
                }
            }).then(function (response) {
                vm.departments = response;
            }).catch(function (error) {
                console.log(error);
            })
        }

        vm.showDepartments = function (deptNo) {
            var departmentNo = deptNo;
            $state.go("C1", {deptNo: departmentNo})
        }
    }

})();